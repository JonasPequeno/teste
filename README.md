    README
Este README fornece instruções básicas sobre como executar um arquivo HTML em seu navegador localmente.

Executando um Arquivo HTML Localmente
Para executar um arquivo HTML localmente em seu navegador, siga os passos abaixo:


### Baixe o Código:
Baixe o código-fonte ou o arquivo HTML que você deseja executar.
Escolha um Navegador:

### Use um navegador da web instalado em seu computador. Os navegadores comuns incluem Google Chrome, Mozilla Firefox, Microsoft Edge e Safari.

### Localize o Arquivo HTML:

Abra o explorador de arquivos e navegue até o local onde o arquivo HTML está salvo.


### Abra o Arquivo HTML:
Clique com o botão direito no arquivo HTML.
Escolha "Abrir com" e selecione o navegador que você deseja usar.

### Visualize no Navegador:
O arquivo HTML será aberto no navegador selecionado, exibindo o conteúdo da página.


### Interaja com a Página:
Ao acessar a pagina aparecerar um campo onde poderá ser inserido o numero.

